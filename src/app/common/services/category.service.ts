import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment.prod';
import {Observable} from 'rxjs';
import {Categories, Services} from '../models/Categories.model';
import {find, map} from 'rxjs/operators';

@Injectable()
export class CategoryService {
    constructor(private http: HttpClient) {}

    getAllCategories(): Observable<Categories[]> {
        return this.http.get(environment.api + 'categories').pipe(
            map((res: Categories[]) => {
                return res;
            })
        );
    }

    getCategoryById(id: number): Observable<Categories> {
        return this.http.get(environment.api + 'categories/' + id).pipe(
            map((res: Categories) => {
                return res;
            })
        );
    }

    getAboutServiceById(categoryId: number, serviceId: number) {
        return this.http.get(environment.api + 'categories/' + categoryId).pipe(
            map((res: Categories) => {
                return res.services.find(
                    (s: Services) => s.id === +serviceId
                );
            })
        );
    }
}
