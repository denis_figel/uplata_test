import { Component, OnInit } from '@angular/core';
import {CategoryService} from '../../common/services/category.service';
import {Services} from '../../common/models/Categories.model';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {

  serviceData: Services;
  constructor(private service: CategoryService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params
        .subscribe((params: Params) => {
            this.service.getAboutServiceById(params['id'], params['serviceId'])
                .subscribe(
                    (res: Services) => {
                        if(res) {
                            this.serviceData = res;
                        } else {
                            console.log('not found');
                        }
                    },
                    (error: Error) => {
                        console.log(error);
                    }
                );
        });

  }

}
