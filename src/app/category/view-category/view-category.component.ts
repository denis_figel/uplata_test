import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {Categories} from '../../common/models/Categories.model';
import {CategoryService} from '../../common/services/category.service';

@Component({
  selector: 'app-view-category',
  templateUrl: './view-category.component.html',
  styleUrls: ['./view-category.component.css']
})
export class ViewCategoryComponent implements OnInit {

    category: Categories;
    constructor(private service: CategoryService, private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.params
            .subscribe((params: Params) => {
                this.service.getCategoryById(params['id'])
                    .subscribe(
                        (res: Categories) => {
                            this.category = res;
                        },
                        (error: Error) => {
                            console.log(error);
                        }
                    );
            });

    }

}
