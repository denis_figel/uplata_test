export interface MenuItems {
    id: number;
    title: string;
    body: string;
}
