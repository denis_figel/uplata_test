import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {HeaderService} from '../common/services/header.service';
import {Subscription} from 'rxjs/internal/Subscription';
import {MenuItems} from '../common/models/MenuItems.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  menuItemsSubscription: Subscription;
  menuItems: MenuItems[] = [];
  constructor(private service: HeaderService) { }

  ngOnInit() {
    this.menuItemsSubscription = this.service.getTopMenu()
        .subscribe(
            (res: MenuItems[]) => {
              this.menuItems = res;
            },
            (error: Error) => {
              console.log(error);
            }
        );
  }

  ngOnDestroy() {
    this.menuItemsSubscription.unsubscribe();
  }

}
