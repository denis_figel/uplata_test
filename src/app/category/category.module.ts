import {NgModule} from '@angular/core';
import {CategoryComponent} from './category.component';
import {CommonModule} from '@angular/common';
import {CategoryRoutesModule} from './category.routes.module';
import {CategoryService} from '../common/services/category.service';
import {ServiceComponent} from './service/service.component';
import {ViewCategoryComponent} from './view-category/view-category.component';

@NgModule({
    declarations: [
        CategoryComponent,
        ServiceComponent,
        ViewCategoryComponent
    ],
    imports: [
        CommonModule,
        CategoryRoutesModule,
    ],
    providers: [CategoryService]
})
export class CategoryModule {}
