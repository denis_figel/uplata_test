import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {Injectable} from '@angular/core';
import { catchError } from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
    constructor(private route: Router) {}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const copiedReq = req.clone();

        return next.handle(copiedReq).pipe(catchError(err => {
            if (err.status === 404) {
                // alert(404);
                this.route.navigate(['not_found']);
            } else if (err.status === 500) {
                // alert(500);
                this.route.navigate(['server_error']);
            } else {
                alert('Error');
            }
            return throwError(err);
        }));
    }
}