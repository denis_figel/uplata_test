import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-popups',
  templateUrl: './popups.component.html',
  styleUrls: ['./popups.component.css']
})
export class PopupsComponent implements OnInit {
    displayLogin = false;
    displayRegister = false;
    rememberMe = false;

    signUpForm: FormGroup;
    accept = false;
  constructor(private router: Router) { }

  ngOnInit() {
      this.signUpForm = new FormGroup({
          'name': new FormControl(null, [Validators.required]),
          'phoneNumber': new FormControl(null),
          'email': new FormControl(null, [Validators.required, Validators.email], this.checkEmailValidation.bind(this)),
          'password': new FormControl(null, [Validators.required]),
          'rePassword': new FormControl(null, [Validators.required, this.comparePasswords.bind(this)]),
          'accept': new FormControl(this.accept, [Validators.required])
      });
  }

  onLogin(form: NgForm): void {
      console.log(form);
      setTimeout(() => {
          // alert('Success');
          this.router.navigate(['/category']);
          this.displayLogin = !this.displayLogin;
      }, 2000);
  }

    comparePasswords(control: FormControl): {[s: string]: boolean} {
        if (this.signUpForm) {
            if (this.signUpForm.get('password').value !== control.value) {
                return {'passwordsDoNotMatch': true};
            }
            return null;
        }
    }

    checkEmailValidation(control: FormControl) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
              if(control.value === 'test@test.com') {
                  resolve({ emailExists: true });
              } else {
                  resolve(null);
              }
            }, 2000);
        });
    }

    onRegister() {
        console.log(this.signUpForm);
        setTimeout(() => {
            alert('Success');
            this.signUpForm.reset();
        }, 2000);
    }

}
