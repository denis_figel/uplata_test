import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {CategoryComponent} from './category.component';
import {ServiceComponent} from './service/service.component';
import {ViewCategoryComponent} from './view-category/view-category.component';

const routes: Routes = [
  {path: '', component: CategoryComponent, children: [
      {path: ':id', component: ViewCategoryComponent},
      {path: ':id/service/:serviceId', component: ServiceComponent}
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutesModule {}
