import { Component, OnInit } from '@angular/core';
import {CategoryService} from '../common/services/category.service';
import {Categories} from '../common/models/Categories.model';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  categories: Categories[] = [];
  constructor(private service: CategoryService) { }

  ngOnInit() {
    this.service.getAllCategories()
        .subscribe(
            (res: Categories[]) => {
              this.categories = res;
            },
            (error: Error) => {
                console.log(error);
            }
        );
  }

}
