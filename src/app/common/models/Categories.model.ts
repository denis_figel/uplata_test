export interface Services {
    id: number;
    title: string;
    body: string;
    categoryId: number;
}
export interface Categories {
    id: number;
    title: string;
    services: Services[];
}
