import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { MenuItems } from '../models/MenuItems.model';
import { map } from 'rxjs/operators';

@Injectable()
export class HeaderService {
    constructor(private http: HttpClient) {}

    getTopMenu(): Observable<MenuItems[]> {
        return this.http.get(environment.api + 'menuItems').pipe(
            map((res: MenuItems[]) => {
                return res;
            })
        );
    }

    getMenuById(id: number): Observable<MenuItems> {
        return this.http.get(environment.api + 'menuItems/' + id).pipe(
            map((res: MenuItems) => {
                return res;
            })
        );
    }
}
