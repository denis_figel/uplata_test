import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuItems } from '../common/models/MenuItems.model';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {
  pageData: MenuItems;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
      this.route.data
          .subscribe(
              (res: MenuItems) => {
                this.pageData = res['page'];
              },
              (error: Error) => {
                  console.log(error);
              }
          );
  }

}
