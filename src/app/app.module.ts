import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { HeaderComponent } from './header/header.component';
import { HeaderService } from './common/services/header.service';
import { RouterModule, Routes } from '@angular/router';
import { PageResolver } from './common/services/page-resolver.service';
import {RequestInterceptor} from './common/services/request.interceptor';
import { ErrorsComponent } from './errors/errors.component';
import { HomeComponent } from './home/home.component';
import { PopupsComponent } from './popups/popups.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'page/:id', loadChildren: './page/page.module#PageModule', resolve: {page: PageResolver} },
    { path: 'category', loadChildren: './category/category.module#CategoryModule'},
    { path: 'not_found', component: ErrorsComponent, data: {message: 'Page not Found.'} },
    { path: 'server_error', component: ErrorsComponent, data: {message: 'Server Error.'} },
    { path: '**', redirectTo: 'not_found' }
];

@NgModule({
  declarations: [
      AppComponent,
      HeaderComponent,
      ErrorsComponent,
      HomeComponent,
      PopupsComponent
  ],
  imports: [
      BrowserModule,
      HttpClientModule,
      FormsModule,
      ReactiveFormsModule,
      RouterModule.forRoot(routes)
  ],
  providers: [
      HeaderService,
      PageResolver,
      {provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
