import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';

import {Observable} from 'rxjs';
import {HeaderService} from './header.service';
import {Injectable} from '@angular/core';
import {MenuItems} from '../models/MenuItems.model';

@Injectable()
export class PageResolver implements Resolve<MenuItems> {
    constructor(private service: HeaderService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<MenuItems> | Promise<MenuItems> | MenuItems  {
        return this.service.getMenuById(route.params['id']);
    }
}
