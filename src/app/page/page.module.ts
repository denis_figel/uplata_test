import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {PageRoutesModule} from './page.routes.module';
import {PageComponent} from './page.component';

@NgModule({
  declarations: [
      PageComponent
  ],
  imports: [
    CommonModule,
    PageRoutesModule,
  ],
  providers: [],
})

export class PageModule {}
